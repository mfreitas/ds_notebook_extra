FROM registry.gitlab.com/mfreitas/ds_notebook_server:latest

WORKDIR /build

#Install Bioconductor Extra Packages
COPY bioconductor_extra.R /build/bioconductor_extra.R
RUN Rscript bioconductor_extra.R 

#Install Python3 Core Packages
COPY requirements_extra.txt /build/requirements_extra.txt
RUN pip3 install -r requirements_extra.txt \
