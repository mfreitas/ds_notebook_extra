#Data Science Notebook - Extra

Jupyter Notebook inside a Docker container with R 3.5.1 and Python 3.6.6.

The notebook server adds to the ds_notebook_server the following  R and Python packages.

R=3.5.1 | Python=3.6.6
-|-
limma | pymzml
edgeR | peakutils
baySeq | 
DESeq | 
genefilter |
qvalue | 
imputeLCMD | 
naniar |

```
docker run -d -p 8888:8888-v "${PWD}":"/data" --name ds_notebookserver \
    ds_notebook_extra:latest  /bin/bash -c "jupyter notebook --ip 0.0.0.0 --no-browser \
    --allow-root --NotebookApp.token='' --notebook-dir='/data'"
```

Open your browser to localhost:8888 to view the notebook.

Bash scripts are provided to start and stop the notebook server.

```
./run_dsnotebook.sh
./stop_dsnotebook.sh
```
