#!/usr/bin/env r

if (!requireNamespace("BiocManager"))
    install.packages("BiocManager")
BiocManager::install()

#Require Package
pkg_list=c(
    "shiny"
)
BiocManager::install(pkg_list)

#Analysis
pkg_list=c(
    "tidyverse"
)
BiocManager::install(pkg_list)

#MS Analysis
pkg_list=c(
    "limma","edgeR","baySeq","DESeq","genefilter",
    "qvalue","imputeLCMD","naniar","MSnbase"
)
BiocManager::install(pkg_list)

#Visualization
pkg_list=c(
    "ggridges","plot3d"
)
BiocManager::install(pkg_list)