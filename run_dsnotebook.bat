#EDIT THE FOLLOWING
SET "docker_image=mfreitas/ds_notebook_extra"
SET "name=ds_notebook_extra"
SET "tag=latest" 

SET "share=/%cd%"
SET "share=%share:\=/%"
SET "share=%share:C:=c%"
SET "share=%share:D:=d%"
SET "docker_bin=docker.exe"

#EDIT THE FOLLOWING. CHANGE TO YOUR PREFERRED HTTP PORT
SET port=8888

echo "Stopping DS Notebook"
%docker_bin% container stop %name%
%docker_bin% rm %name%

echo "Starting up DS Notebook"
%docker_bin% login registry.gitlab.com
%docker_bin% pull registry.gitlab.com/%docker_image%:%tag%
%docker_bin% run -d -p %port%:%port% -v "%share%":"/data" --name %name% registry.gitlab.com/%docker_image%:%tag% /bin/bash -c "jupyter notebook --ip 0.0.0.0 --no-browser --allow-root --NotebookApp.token='' --notebook-dir='/data'"
 
timeout 5
 
start "" http://192.168.99.100:%port%
